package main.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Category", schema = "cafe")
public class CategoryE {
    private int categoryId;
    private String name;
    private String description;

    @Id
    @Column(name = "Category_ID", nullable = false)
    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    @Basic
    @Column(name = "Name", nullable = false, length = 45)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "Description", nullable = true, length = 1000)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryE categoryE = (CategoryE) o;
        return categoryId == categoryE.categoryId &&
                Objects.equals(name, categoryE.name) &&
                Objects.equals(description, categoryE.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(categoryId, name, description);
    }
}
