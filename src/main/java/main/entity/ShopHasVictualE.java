package main.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Shop_has_Victual", schema = "cafe")
public class ShopHasVictualE {
    private int shopHasVictualId;
    private int price;

    @Id
    @Column(name = "Shop_has_Victual_ID", nullable = false)
    public int getShopHasVictualId() {
        return shopHasVictualId;
    }

    public void setShopHasVictualId(int shopHasVictualId) {
        this.shopHasVictualId = shopHasVictualId;
    }

    @Basic
    @Column(name = "Price", nullable = false)
    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShopHasVictualE that = (ShopHasVictualE) o;
        return shopHasVictualId == that.shopHasVictualId &&
                price == that.price;
    }

    @Override
    public int hashCode() {
        return Objects.hash(shopHasVictualId, price);
    }
}
