package main.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "Item", schema = "cafe")
public class ItemE {
    private int itemId;
    private Timestamp buyTimestamp;
    private Timestamp payoffTimestamp;

    @Id
    @Column(name = "Item_ID", nullable = false)
    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    @Basic
    @Column(name = "Buy_Timestamp", nullable = false)
    public Timestamp getBuyTimestamp() {
        return buyTimestamp;
    }

    public void setBuyTimestamp(Timestamp buyTimestamp) {
        this.buyTimestamp = buyTimestamp;
    }

    @Basic
    @Column(name = "Payoff_Timestamp", nullable = true)
    public Timestamp getPayoffTimestamp() {
        return payoffTimestamp;
    }

    public void setPayoffTimestamp(Timestamp payoffTimestamp) {
        this.payoffTimestamp = payoffTimestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemE itemE = (ItemE) o;
        return itemId == itemE.itemId &&
                Objects.equals(buyTimestamp, itemE.buyTimestamp) &&
                Objects.equals(payoffTimestamp, itemE.payoffTimestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(itemId, buyTimestamp, payoffTimestamp);
    }
}
