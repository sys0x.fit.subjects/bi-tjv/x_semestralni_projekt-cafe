package main.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Shop", schema = "cafe")
public class ShopE {
    private int storeId;
    private String name;

    @Id
    @Column(name = "Store_ID", nullable = false)
    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    @Basic
    @Column(name = "Name", nullable = false, length = 45)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShopE shopE = (ShopE) o;
        return storeId == shopE.storeId &&
                Objects.equals(name, shopE.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(storeId, name);
    }
}
