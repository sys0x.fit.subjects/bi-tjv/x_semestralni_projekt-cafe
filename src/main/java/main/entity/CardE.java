package main.entity;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "Card", schema = "cafe")
public class CardE {
    private int cardId;
    private int cardNumber;
    private String name;
    private Date expiration;
    private int cvv;

    @Id
    @Column(name = "Card_ID", nullable = false)
    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    @Basic
    @Column(name = "Card_Number", nullable = false)
    public int getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Basic
    @Column(name = "Name", nullable = false, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "Expiration", nullable = false)
    public Date getExpiration() {
        return expiration;
    }

    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }

    @Basic
    @Column(name = "CVV", nullable = false)
    public int getCvv() {
        return cvv;
    }

    public void setCvv(int cvv) {
        this.cvv = cvv;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CardE cardE = (CardE) o;
        return cardId == cardE.cardId &&
                cardNumber == cardE.cardNumber &&
                cvv == cardE.cvv &&
                Objects.equals(name, cardE.name) &&
                Objects.equals(expiration, cardE.expiration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardId, cardNumber, name, expiration, cvv);
    }
}
