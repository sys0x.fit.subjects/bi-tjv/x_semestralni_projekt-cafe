package main.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Address", schema = "cafe")
public class AddressE {
    private int addressId;
    private String street;
    private String city;
    private String other;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "Address_ID", nullable = false)
    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    @Basic
    @Column(name = "Street", nullable = false, length = 45)
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Basic
    @Column(name = "City", nullable = false, length = 45)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "Other", nullable = true, length = 4000)
    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddressE addressE = (AddressE) o;
        return addressId == addressE.addressId &&
                Objects.equals(street, addressE.street) &&
                Objects.equals(city, addressE.city) &&
                Objects.equals(other, addressE.other);
    }

    @Override
    public int hashCode() {
        return Objects.hash(addressId, street, city, other);
    }
}
