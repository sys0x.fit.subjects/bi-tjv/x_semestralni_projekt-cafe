package main.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Victual", schema = "cafe")
public class VictualE {
    private int victualId;
    private String name;

    @Id
    @Column(name = "Victual_ID", nullable = false)
    public int getVictualId() {
        return victualId;
    }

    public void setVictualId(int victualId) {
        this.victualId = victualId;
    }

    @Basic
    @Column(name = "Name", nullable = false, length = 45)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VictualE victualE = (VictualE) o;
        return victualId == victualE.victualId &&
                Objects.equals(name, victualE.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(victualId, name);
    }
}
