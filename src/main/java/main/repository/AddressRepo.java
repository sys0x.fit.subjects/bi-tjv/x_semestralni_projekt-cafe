package main.repository;

import main.entity.AddressE;
import org.springframework.data.repository.CrudRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called addressRepo
// CRUD refers Create, Read, Update, Delete

public interface AddressRepo extends CrudRepository<AddressE, Integer> {
}
