package main.repository;

import main.entity.ShopE;
import org.springframework.data.repository.CrudRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called shopRepo
// CRUD refers Create, Read, Update, Delete

public interface ShopRepo extends CrudRepository<ShopE, Integer> {
}
