package main.repository;

import main.entity.CardE;
import org.springframework.data.repository.CrudRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called cardRepo
// CRUD refers Create, Read, Update, Delete

public interface CardRepo extends CrudRepository<CardE, Integer> {
}
