package main.repository;

import main.entity.VictualE;
import org.springframework.data.repository.CrudRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called victualRepo
// CRUD refers Create, Read, Update, Delete

public interface VictualRepo extends CrudRepository<VictualE, Integer> {
}
