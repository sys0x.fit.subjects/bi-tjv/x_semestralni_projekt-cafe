package main.repository;

import main.entity.CustomerE;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called customerRepo
// CRUD refers Create, Read, Update, Delete
@Repository
public interface CustomerRepo extends CrudRepository<CustomerE, Integer> {
}
