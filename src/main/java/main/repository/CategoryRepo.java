package main.repository;

import main.entity.CategoryE;
import org.springframework.data.repository.CrudRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called categoryRepo
// CRUD refers Create, Read, Update, Delete

public interface CategoryRepo extends CrudRepository<CategoryE, Integer> {
}
